package com.aquant.pipe;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;
import org.apache.commons.math3.stat.StatUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Driver {

    public static void main(String[] args)  throws Exception {
        ArrayList<String> Code = new ArrayList<String>();
        ArrayList<String> Attr = new ArrayList<String>();
        ArrayList<String> Date = new ArrayList<String>();
        ArrayList<String> Query = new ArrayList<String>();

        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost/omnia", "postgres", "****");
        con.setAutoCommit(false);
        Statement stmt = con.createStatement();

        String tmp = "select * from trends where attr ='" + args[0] + "';";


        ResultSet rs = stmt.executeQuery(tmp);
        while (rs.next()){
            JsonParser jParser = new JsonParser();
            Code.add(rs.getString(1));
            Attr.add(rs.getString(2));
            Date.add(rs.getString(4));

            JsonArray linear = curve_fitting(jParser.parse(rs.getString(3)).getAsJsonArray(), 1);
            JsonArray quadratic = curve_fitting(jParser.parse(rs.getString(3)).getAsJsonArray(), 2);
            JsonObject basic = basic(jParser.parse(rs.getString(3)).getAsJsonArray());

            Query.add ("insert into eavt select '" + rs.getString(1) + "', '"+rs.getString(2)+"_linear"+"', '" + linear + "', '" + rs.getString(4) + "'"
                    + "union select '" + rs.getString(1) + "', '"+rs.getString(2)+"_quadratic"+"', '" + quadratic + "', '" + rs.getString(4) + "'"
                    + "union select '" + rs.getString(1) + "', '"+rs.getString(2)+"_basic"+"', '" + basic + "', '" + rs.getString(4) + "';");

        }

        for (int i =0; i < Query.size(); i ++){
            stmt.execute(Query.get(i));
        }
        con.commit();

    }


    public static JsonArray curve_fitting (JsonArray jArray, Integer degree){
        JsonArray ans = new JsonArray();

        WeightedObservedPoints obs = new WeightedObservedPoints();
        for ( int i = 0; i < jArray.size(); i ++){
            if (!jArray.get(i).isJsonNull()){
                obs.add(i,jArray.get(i).getAsDouble());
            }

        }
        if (obs.toList().size()>0){
            PolynomialCurveFitter fitter = PolynomialCurveFitter.create(degree);
            double[] coeff = fitter.fit(obs.toList());


            for ( int i = 0; i < coeff.length; i  ++){
                ans.add(coeff[i]);
            }
            return ans;
        }else
            return ans;
    }

    public static JsonObject basic (JsonArray jArray){
//		JsonArray ans = new JsonArray();
        JsonObject ans = new JsonObject();

        for ( int i = jArray.size()-1; i >=0; i -- ){
            if (jArray.get(i).isJsonNull()){
                jArray.remove(i);
            }

        }
//		System.out.println(jArray);;
        Gson gson = new Gson();
        double[] oList = gson.fromJson(jArray, double[].class);
        double[] pList = plus(oList);
        double sum     = StatUtils.sum(oList);
        ans.addProperty("sum", sum);
        double average = StatUtils.mean(oList);
        ans.addProperty("average",average);
        double variance = StatUtils.variance(oList);
        ans.addProperty("variance",variance);
        double minimum = StatUtils.min(oList);
        ans.addProperty("minimum",minimum);
        double maximum = StatUtils.max(oList);
        ans.addProperty("maximum",maximum);
        double pop_var = StatUtils.populationVariance(oList);
        ans.addProperty("pop_var",pop_var);
        double sum_log = StatUtils.sumLog(pList);
        ans.addProperty("sum_log",sum_log);
        double sum_sqr = StatUtils.sumSq(oList);
        ans.addProperty("sum_sqr",sum_sqr);
        double geo_avg = StatUtils.geometricMean(pList);
        ans.addProperty("geo_avg",geo_avg);
        double fst_qrt = StatUtils.percentile(oList, 25);
        ans.addProperty("fst_qrt",fst_qrt);
        double lst_qrt = StatUtils.percentile(oList, 75);
        ans.addProperty("lst_qrt",lst_qrt);
        double fst_pct = StatUtils.percentile(oList, 10);
        ans.addProperty("fst_pct",fst_pct);
        double lst_pct = StatUtils.percentile(oList, 90);
        ans.addProperty("lst_pct",lst_pct);
        return ans;
    }

    private static double[] plus(double[] oList) {
        double[] ans = new double[oList.length];
        for (int i = 0; i < ans.length; i ++)
            ans[i]= oList[i]+1;
        return ans;
    }


}

